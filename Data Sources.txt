Books:
 - Goodreads: no (closed API)
 - Wikidata: possibly (open+free)
 - GoogleBooks
 - OpenLibrary : Has downloadable data-base; someone has already written something to download it:https://github.com/LibrariesHacked/openlibrary-search

Ways to connect Book and Movie metadata:
 - Connect metadata on books and movies by genre, such as linking a romance novel with a romantic comedy film.
 - Use metadata on authors to link books and movies adapted from their works.
 - Use metadata on actors and directors to connect books and movies they have starred in or directed.
 - Connect metadata on themes and topics to link books and movies with similar themes or subject matter.
 - Use metadata on release dates to link books and movies that were released in the same year or within a certain time period.
 - Connect metadata on awards and accolades to link books and movies that have won or been nominated for the same awards.
 - Use metadata on locations to link books and movies that are set in the same location or region.
 - Connect metadata on ratings and reviews to link books and movies with similar ratings or positive reviews.
