# Basic web app tutorial

This repository has a very basic example of backend and frontend communication. The backend is written in Python using the Flask framework and the frontend is HTML with plain JavaScript.

This project is explain on my Blog at: [Python backend with JavaScript frontend: how to](https://tms-dev-blog.com/python-backend-with-javascript-frontend-how-to/)

## Requirements

 - [Python](https://www.python.org/downloads/)
 - [Node JS](https://nodejs.org/en/download/)

## Set up

Install the requirements for the backend by first creating a virtual environment in the backend directory:

```
cd backend
python -m venv venv
```
Then install the dependencies:

```
pip install -r "requirements.txt"
```

In another terminal, run the following:
```
cd frontend
npm install
```

## Running the project

Start the backend server using the following command in the first terminal:

```
python app.py
```

Start the frontend in the second terminal:

```
npx http-server -p 8001
```

Navigate using a browser to the address the frontend is hosted at. This is `http://localhost:8080` by default.
