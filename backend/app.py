from flask import Flask, request
import flask
import json
from flask_cors import CORS
#import wikidataAPI as wapi

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return "Hello, World!"

@app.route('/users', methods=["GET", "POST"])
def users():
    print("users endpoint reached...")
    if request.method == "GET":
        with open("users.json", "r") as f:
            data = json.load(f)
            data.append({
                "username": "user4",
                "pets": ["hamster"]
            })

            return flask.jsonify(data)
    if request.method == "POST":
        received_data = request.get_json()
        print(f"received data: {received_data}")
        message = received_data['data']
        return_data = {
            "status": "success",
            "message": f"received: {message}"
        }
        return flask.Response(response=json.dumps(return_data), status=201)

@app.route('/filters', methods=["POST"])
def getFilters():
    if request.method != "POST":
        print("Invalid request method on /filters")

    # Load filters from file
    with open("filters.json", "r") as f:
        data = json.load(f)

    received_data = request.get_json()
    message = received_data['checkedFilters']

    allFilters = set()
    for type in message:
        allFilters.update(data[type])

    response = {
        "type": "filters",
        "filters": list(allFilters)
    }

    return flask.Response(status=200, response=json.dumps(response))

@app.route('/search', methods=["POST"])
def getSearch():
    if request.method != "POST":
        print("Invalid request method on /search")

    received_data = request.get_json()
    checkedFilters = received_data['checkedFilters']
    userFilters = received_data['userFilters']

    print(checkedFilters)
    print(userFilters)

    response = {
        "type": "results"
    }

    return flask.Response(status=200, response=json.dumps(response))
    


if __name__ == "__main__":
    app.run("127.0.0.1", 8000)