from wikidataintegrator import wdi_core

def totalquery(director='',title='',genre='',castmember='',duration='',startrangepub='',endrangepub=''):#, title = '', genre = '', publicationstart = '', publicationend = '' . duration = '' ): 
    string1 = "SELECT DISTINCT ?item ?title ?directorname ?genre ?genrename ?castmember ?castmembername ?duration ?publicationdate WHERE{ \n"
    string1a= "VALUES ?type {wd:Q11424} ?item wdt:P31 ?type .\n"
    string2 = title_querybuilder(title) 
    string3 = director_querybuilder(director)
    string4 = genre_querybuilder(genre) 
    string5 = duration_querybuilder(duration) 
    string6 = publicationdate_querybuilder(startrangepub,endrangepub) 
    stringcast ='' #  castmember_querybuilder(castmember) castmember does not work yet
    labelstring = "SERVICE wikibase:label {bd:serviceParam wikibase:language \"en\" .} \n"
    endstring = "} \n LIMIT 100"
    return string1+string1a+string2+string3+string4 +string5+string6+ stringcast+ labelstring +endstring

def director_querybuilder(directorname2): #Creates string and possible filter for director. 
    querystringstart = '?item wdt:P57 ?director. \n'
    directorname     = '?director rdfs:label ?directorname. \n'
    if directorname2 == '': #not chosen to pick a director
        dir_querystringend = ''
   
    else: 
        dir_querystringend = 'FILTER(REGEX(?directorname, "{dir_name}", "i"))\n'.format(dir_name=directorname2) 
    return  querystringstart + directorname + dir_querystringend
        
def title_querybuilder(title):  #Creates string and possible filter for title
    stringstart = "OPTIONAL { ?item wdt:P1476 ?title.}\n" 
    if title == '': 
        title_querystringend = ''
    else: 
        title_querystringend = 'FILTER(REGEX(?title, "{title_name}", "i"))\n'.format(title_name =title) 
    return stringstart + title_querystringend 

def genre_querybuilder(genreinput) : #Creates string and filter for genre
    genre_stringstart = 'OPTIONAL {?item wdt:P136 ?genre.} \n'
    genre_name        = 'OPTIONAL {?genre rdfs:label ?genrename.} \n'
    genre_namefilter  = 'filter(langMatches(lang(?genrename),"EN"))\n'
    if genreinput == '': 
        genre_stringend = ''
    else: 
        genre_stringend = 'FILTER(REGEX(\'?genre_name, "{genrefilter}", "i"))\n'.format(genrefilter = genreinput) 
    return genre_stringstart + genre_name +genre_namefilter + genre_stringend

def castmember_querybuilder(castmemberinput) : #Creates string to get castmembers, not yet working
    castmember_start =  'OPTIONAL {?item wdt:P161 ?castmember.} \n'
    castmember_name =  'OPTIONAL {?castmember rdfs:label ?castmembername.} \n'
    castmember_name_namefilter  = 'filter(langMatches(lang(?castmembername),"EN"))\n'
    if castmemberinput =='':
        castmember_end = '' 
    else: 
        castmember_end = 'FILTER(REGEX(\'?castmember_name, "{castmemberfilter}", "i"))\n'.format(castmemberfilter =castmemberinput) 
    return castmember_start + castmember_name+ castmember_name_namefilter +castmember_end


def  duration_querybuilder(duration) : #Creates string to get the duration
    duration_start = 'OPTIONAL {?item wdt:P2047 ?duration.} \n'
    return duration_start 

def publicationdate_querybuilder(startrangeinput,endrangeinput) : #Creates string with possible filter for publication date
    publicationdate_start = 'OPTIONAL {?item wdt:P577 ?publicationdate.} \n'
    if startrangeinput == '' : 
        publication_filterstring= '' 
    else: 
        publication_filterstring= 'FILTER("{filterstart}" ^^xsd:dateTime <= ?publicationdate && ?publicationdate < "{filterend}"^^xsd:dateTime).\n'.format(filterstart= startrangeinput,filterend = endrangeinput)
    return publicationdate_start +publication_filterstring


abc = totalquery(title = 'star wars',startrangepub='2005-01-01',endrangepub='2006-01-01' )


class WikidataAPI:
    def __init__(self):
        self.nop = "nop"

    def getQuery(self, query, endpoint='https://query.wikidata.org/sparql'):
        return wdi_core.WDItemEngine.execute_sparql_query(query, endpoint=endpoint)


if __name__ == "__main__":
    api = WikidataAPI()
    query = '''
    SELECT DISTINCT ?item ?itemLabel WHERE {
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
        {
            SELECT DISTINCT ?item WHERE {
                {
                    ?item p:P31 ?statement0.
                    ?statement0 (ps:P31/(wdt:P279*)) wd:Q2431196.
                }
                UNION
                {
                    ?item p:P31 ?statement1.
                    ?statement1 (ps:P31/(wdt:P279*)) wd:Q47461344.
                }
                UNION
                {
                    ?item p:P31 ?statement2.
                    ?statement2 (ps:P31/(wdt:P279*)) wd:Q2188189.
                }
            }
            LIMIT 100
        }
    }
    '''

    result = api.getQuery(query)
    print(result)
